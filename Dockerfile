FROM alpine as installer

ARG tarball=https://build.funtoo.org/funtoo-current/x86-64bit/generic_64/stage3-latest.tar.xz
ENV TARBALL $tarball

RUN apk add wget

COPY scripts/chroot.sh /opt/funtoo/chroot.sh
RUN sh /opt/funtoo/chroot.sh

FROM scratch as base
COPY --from=installer /opt/funtoo/chroot /
COPY scripts/update.sh /opt/update.sh
COPY templates/make.conf /opt/make.conf

# TODO: For some reason, `docker build --build-arg nproc=6` does not set the
# $nproc value to 6. For now, I'll hardcode this to 6.
ARG nproc=6

ENV NPROC $nproc

RUN sh /opt/update.sh
RUN rm -rf /opt && mkdir /opt

FROM scratch as clean
COPY --from=base / /

WORKDIR /root
CMD ["/bin/bash"]
