#! /usr/bin/env sh

main()
{
	run mkdir -p /opt/funtoo
	run cd /opt/funtoo

	run wget -q -O funtoo.tar.xz "$TARBALL"

	run mkdir -p /opt/funtoo/chroot
	run tar xpf /opt/funtoo/funtoo.tar.xz -C /opt/funtoo/chroot

	run rm -rf /opt/funtoo/chroot/boot
	run rm -rf /opt/funtoo/usr/src/*
	run rm -rf /opt/funtoo/lib64/modules
}

run()
{
	printf "   ---> [%s] %s\n" "$(date '+%T')" "$*"
	$* >> /dev/null 2>&1
}

main "$@"
