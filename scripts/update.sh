#! /usr/bin/env sh

main()
{
	EMERGE="emerge --ask=n --quiet-build"

	run sed -i -e 's/#rc_sys=""/rc_sys="docker"/g' /etc/rc.conf
	run printf "%s" "UTC" > etc/timezone
	envsubst < /opt/make.conf > /etc/portage/make.conf

	SILENCE=1 run ego sync

	run $EMERGE -uDN @world
	run $EMERGE -uDN @preserved-rebuild
	run $EMERGE --depclean
	#run $EMERGE --rage-clean sys-kernel/debian-sources

	run rm -rf /var/git
	run rm -rf /var/tmp && mkdir /var/tmp
	run rm -rf /tmp && mkdir /tmp
	run rm -rf /var/log && mkdir /var/log
	run rm -f /etc/portage/make.conf
}

run()
{
	printf "   ---> [%s] %s\n" "$(date '+%T')" "$*"

	if [ ! -z "$SILENCE" ]
	then
		$* >> /dev/null 2>&1
	else
		$*
	fi
}

main "$@"
